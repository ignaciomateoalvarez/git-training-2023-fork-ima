# Ejercicios

Este repositorio cuenta con 2 ramas principales, develop y main, ya creadas.

 - Clonar el repositorio.
 - Una vez clonado el repositorio, creen una rama basada en develop llamada feature/nombre_apellido (su nombre y su apellido).
 - Modificar el archivo Participantes y agregar en él su nombre y apellido.
 - Generar un [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html). Este mismo debe indicar:
   - Como source branch la rama nueva creada por ustedes.
   - Como target branch develop.
   - Deben asignar a Benjamin Ciancio como reviewers.

Una vez creado, debe ser aprobado por las personas asignadas para revisarlo. A su vez, puede que éstas le hagan comentarios o sugerencias sobre los cambios que ustedes agregaron para proponerles mejoras o corregirles algo.

## Para valientes

- Realizar un fork (bifurcación) del repositorio. Esto lo que va a hacer es generar un nuevo repositorio dentro de sus repositorios personales, el cual queda enlazado de alguna forma al que creamos para esta actividad. Consejo: Leer [esta guía](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) sobre cómo hacer un fork.
- Actualizar la rama develop del repositorio forkeado para que ahora incluya los commits que han sido mergeados en el repositorio base. Consejo: pueden seguir [esta guía](https://docs.github.com/es/free-pro-team@latest/github/getting-started-with-github/fork-a-repo#paso-3-configurar-git-para-sincronizar-tu-bifurcaci%C3%B3n-con-el-repositorio-original-spoon-knife).
- Generar una release llamada release/0.2.0 utilizando git-flow y generar la tag 0.2.0. Luego finalizar dicha release. Revisar si la operación fue exitosa:
  - ¿El código de develop, está incluido en la rama main?
  - ¿Se generó la tag 0.2.0?

- Ahora debemos abrir un hotfix/0.2.1, ya que ahora necesitamos que en el archivo Participantes se agregue la fecha del día de hoy en el archivo. Los pasos a seguir son:
  - Crear el hotfix utilizando git-flow.
  - Modificar el archivo Participantes agregando la fecha de hoy.
  - Finalizar el hotfix creando la tag 0.2.1.
  - El archivo README de la rama main fue actualizado?

Consejo: Recordar luego de finalizar sus releases o hotfix de hacer push tanto a main como a develop, parándose en cada rama y ejecutando git push.
